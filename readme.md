# Kubernetes Workshop #
Maintained by Mario Hammer

## Prepare the pi to be a K8s node##
This repo also contains important information about hypriotOS/k8s setup. See this chapter.
The basic rpi setup is to copy the hypriotOS image to an sd card, and then modify the device-init.yaml for the most important configuration.

### Edit device-init.yaml ###
1. Copy this file to the top level of each raspian sd card
2. Do the network configuration in the file
   * Host name
   * Static IP
   * WLAN SSID and Key

### Set up nodes ###
1. Boot the rpi
2. Execute bootup.sh. This will install k8s and do required setup.
3. Execute on master `kubeadm init --pod-network-cidr 10.244.0.0/16`
   * This will output a token and also a join command for nodes
4. Execute on each node `kubeadm join --token=<token> <master ip>`
5. See https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/ for more details

# Logicline LED Blinker #

* This blinker works with Blinkt! LED: 		https://shop.pimoroni.com/products/blinkt
* It uses the library written by Jimbo: 		https://github.com/hackerjimbo/PiJava/blob/master/Jimbo/Boards/com/pimoroni/Blinkt.java
* It also uses docker-client from spotify: 	https://github.com/spotify/docker-client


## Build ##
* Compile option 1: By calling "gradle shadow". This will output the fat-jar to build/libs 
* Compile option 2: By calling "gradle copyFiles", the fat-jar will be copied to a folder /media/sf_VMEx2 (my exchange folder for virtualbox)

## Installation ##
### 1. Copy jar to rpi ###
Copy the jar to a place of your choice, e.g.

```bash
/home/user/blinkled/blinkled-1.0-all.jar
```
You can do that comfortably by using a tool like WinSCP.

### 2. Add jar to etc/rc.local ###

Add the following line to /etc/rc.local.

```bash
# This starts the blinkLED driver, see https://bitbucket.org/logicline/kubernetes-hacknight
java -jar /home/user/blinkled/blinkled-1.0-all.jar
```
Adding to rc.local will start the jar at the end of each runlevel (except 0 and 6).


## Operation: ##

### Start ###

* Start by "sudo java -jar <path-to-jar> 

The server starts by listening on localhost:5050.

If the server throws a lot of stacktraces regarding libjffi-1.2.so. 
You have to recompile the library and copy it in the respective folder.

```bash
git clone https://github.com/jnr/jffi.git
cd jffi
ant jar
sudo cp build/jni/libjffi-1.2.so /usr/lib/libjffi-1.2.so
```

### Connection ###
For getting a connection to localhost from inside a running container, you need to connect to the IP of the ethernet bridge adapter (docker0). Its IP usually is 172.17.0.1 - this is also the IP that the LED driver binds to. 
So to reach it inside a container, connect to http://172.17.0.1:5050.

###Register for a container LED###
When a container wants a LED, it has to POST /container. The body must look like the following JSON. This will check for the docker container with id <docker id> with docker-client. If it is present, it gets a LED with the specified rgb-color.:
```
#!json
{
	"id":"<docker id>",
	"color":"rrr-ggg-bbb"
}
```

###Blink LED###
When the led shall blink, do a GET on /blink/<docker-id>, using exactly the same docker id supplied for registration.

###Unregister###
There's no need to unregister manually. The app will be informed about container destruction via docker event stream, and unregister the used LED.