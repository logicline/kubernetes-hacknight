import com.spotify.docker.client.messages.Container;

/**
 * Created by mario on 6/26/17.
 */
public class ContainerWrapper {

    public int offTick;
    public enum Status {Started, Dying};

    public final String[] color;
    public Status status;
    private final Container container;

    public ContainerWrapper(String[] color, Container container) {
        this.color = color;
        this.container = container;
        this.status = Status.Started;
        this.offTick = 0;
    }

    public Container getContainer() {
        return container;
    }
}
