/**
 * Created by mario on 6/21/17.
 */
public class ColourCalculator {

    private final String[] color;

    public ColourCalculator(String[] color) {
        this.color = color;
    }

    public int getRed(){
        return Integer.valueOf(color[0]);
    }

    public int getGreen(){
        return Integer.valueOf(color[1]);
    }

    public int getBlue(){
        return Integer.valueOf(color[2]);
    }

    @Override
    public String toString() {
        return "R:"+getRed()+"-G:"+getGreen()+"-B:"+getBlue();
    }
}
