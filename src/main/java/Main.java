import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.EventStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by mario on 7/20/16.
 */
public class Main {

    public enum ContainerStatus {Running, Dying, Dead}

    private final static LogiclineBlinkt blinkt = new LogiclineBlinkt();
    private final static Logger logger = LoggerFactory.getLogger(Main.class);
    private final static ScheduledExecutorService service = Executors.newScheduledThreadPool(2);

    private static synchronized Map<String, ContainerWrapper> getContainers(){
        return containers;
    }
    private final static Map<String, ContainerWrapper> containers = new LinkedHashMap<>();

    public static void main(String[] args) throws Exception {

        logger.debug("run!");
        final DockerClient docker = DefaultDockerClient.fromEnv().build();

        RatpackServer.start(s -> s
            .serverConfig(c -> c
                    .baseDir(BaseDir.find())
                    .env())
            .handlers(c -> c
                    .files(f -> f.dir("public"))
                    .get("blink/:containerId", ctx -> {
                        String containerId = ctx.getAllPathTokens().get("containerId");
                        logger.debug("received id: " + containerId);
                        for(String id : containers.keySet()){
                            logger.debug("checking: " + id);
                            if(containerId.startsWith(id) || id.startsWith(containerId)) {
                                containers.get(id).offTick = 2;
                                ctx.getResponse().status(200);
                                ctx.getResponse().send();
                                return;
                            }
                        }
                        ctx.getResponse().status(404);
                        ctx.getResponse().send("Id not found");
                    })
                    .post("container", ctx -> {
                        ctx.getRequest().getBody().then(typedData -> {
                            JsonObject body = new JsonParser().parse(typedData.getText()).getAsJsonObject();
                            if(!body.has("id") || !body.has("color") || body.get("color").getAsString().split("-").length != 3){
                                ctx.getResponse().status(400);
                                ctx.getResponse().send("Body must contain id (valid running docker id) and color (r-g-b), i.e.: {\"id\":\"123\", \"color\":\"255-0-0\"}");
                                return;
                            }
                            String id = body.get("id").getAsString();
                            String[] color = body.get("color").getAsString().split("-");
                            List<Container> cons = docker.listContainers(DockerClient.ListContainersParam.filter("id", id));
                            if(cons.isEmpty()) {
                                logger.info("container is with id " + id + " can't be found. Skipping");
                                ctx.getResponse().status(400);
                                ctx.getResponse().send("Container with id " + id + " was not found as running container");
                                return;
                            }
                            Container container = cons.get(0);
                            getContainers().put(container.id(), new ContainerWrapper(color, container));
                            logger.info("added container with id " + id);
                            logger.info("color: " + new ColourCalculator(color).toString());
                            ctx.getResponse().status(200).send();
                        });
                    })
            ));

        /*logger.info("running containers: "+ docker.info().containersRunning());
        for(Container con : docker.listContainers(DockerClient.ListContainersParam.withStatusRunning())){
            logger.info("Id" + con.id());
            logger.info("Image" + con.image());

            ImmutableMap<String, AttachedNetwork> networks = con.networkSettings().networks();
            for(String name : networks.keySet()){
                AttachedNetwork nw = networks.get(name);
                logger.info("Name: " + name);
                logger.info("IP: " + nw.ipAddress());
            }

            logger.info("PortMappings:");
            for(Container.PortMapping mapping : con.ports()){
                logger.info("IP: " + mapping.ip());
                logger.info("public port: " + mapping.publicPort());
                logger.info("private port: " + mapping.privatePort());
                logger.info("----");
            }
        }
        logger.debug("adding schedules");
*/
        service.scheduleAtFixedRate(new LEDUpdater(), 100, 200, TimeUnit.MILLISECONDS);
        service.scheduleAtFixedRate(new EventPoller(docker), 0, 100, TimeUnit.MILLISECONDS);


        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                logger.info("Inside Shutdown Hook");
                docker.close();
            }
        });

    }

    static class ContainerHandler {

        public void onStart(Container container){
            logger.info("container created: " + container.image());

        }

        public void onDie(String containerId){
            if(!getContainers().containsKey(containerId)) {
                logger.info("unknown container killed: " + containerId);
                return;
            }
            service.schedule(new ContainerRemover(containerId), 500, TimeUnit.MILLISECONDS);
            ContainerWrapper containerWrapper = getContainers().get(containerId);
            Container container = containerWrapper.getContainer();
            containerWrapper.status = ContainerWrapper.Status.Dying;
            logger.info("container killed: " + container.image());
        }
    }

    static class EventPoller implements Runnable {
        private final DockerClient docker;
        private final EventStream stream;
        private final ContainerHandler handler;

        EventPoller(DockerClient docker) throws DockerException, InterruptedException {
            this.docker = docker;
            this.stream = docker.events();
            handler = new ContainerHandler();
        }

        @Override
        public void run() {
            if(!stream.hasNext()) {
                return;
            }

            stream.forEachRemaining(event -> {
                if(event.type() == Event.Type.CONTAINER){
                    try {
                        if(event.action().equals("start")) {
                            List<Container> cons = docker.listContainers(DockerClient.ListContainersParam.filter("id", event.actor().id()));
                            if(cons.isEmpty()) {
                                logger.info("container is with id " + event.actor().id() + " can't be found. Skipping");
                                return;
                            }

                            Container container = cons.get(0);
                            if(!container.image().startsWith("logicline") && !container.image().startsWith("hypriot/rpi-alpine-scratch")){
                                logger.info("Skipped container with image " + container.image());
                                return;
                            }
                            handler.onStart(container);
                        } else if(event.action().equals("die")) {
                            handler.onDie(event.actor().id());
                        }

                    } catch (Exception e) {
                        logger.error("Error:", e);
                    }
                }
            });
        }
    }

    private static class LEDUpdater implements Runnable {

        @Override
        public void run() {
            try {
                blinkt.updateLEDS(getContainers());
            } catch (Throwable e){
                logger.error("Error with blinkt: ", e);
            }
        }
    }

    private static class ContainerRemover implements Runnable {
        private final String containerId;

        public ContainerRemover(String containerId) {
            this.containerId = containerId;
        }

        @Override
        public void run() {
            logger.debug("Trying to remove container: " + containerId);
            if(!getContainers().containsKey(containerId)){
                logger.error("Could not find containerId for removal: " + containerId);
            }

            getContainers().remove(containerId);
        }
    }
}
