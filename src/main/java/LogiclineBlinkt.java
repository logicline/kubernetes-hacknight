import Jimbo.Boards.com.pimoroni.Blinkt;

import java.util.Map;

/**
 * Created by mario on 6/26/17.
 */
public class LogiclineBlinkt {
    private final Blinkt blinkt = new Blinkt();

    public void updateLEDS(Map<String, ContainerWrapper> containers){
        for(Integer i = 0; i < Integer.valueOf(blinkt.getMax().getX()); i++){
            blinkt.set(i, 0, 0, 0, 0);
        }

        Integer i = 0;
        for(ContainerWrapper wrapper : containers.values()){
            if(i >= Integer.valueOf(blinkt.getMax().getX())) break;
            if(wrapper.status == ContainerWrapper.Status.Dying) {
                blinkt.set(i, 255, 0, 0, 31);
            } else {
                if(wrapper.offTick > 0){
                    wrapper.offTick--;
                    blinkt.set(i, 0, 0, 0, 0);
                } else {
                    ColourCalculator calc = new ColourCalculator(wrapper.color);
                    blinkt.set(i, calc.getRed(), calc.getGreen(), calc.getBlue(), 20);
                }
            }
            i++;
        }
        blinkt.show();
    }
}
