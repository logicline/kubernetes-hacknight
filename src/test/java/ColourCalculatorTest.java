import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mario on 6/21/17.
 */
public class ColourCalculatorTest {

    private final static Logger logger = LoggerFactory.getLogger(ColourCalculatorTest.class);


    @Test
    public void testColour(){
        String imageName1 = "logicline/testHackathon1";
        String imageName2 = "logicline/testHackathon1";
        String imageName3 = "logicline/2testHackathon3";
        String imageName4 = "logicline/brezelWurst";

        logger.info(new ColourCalculator(imageName1).toString());
        logger.info(new ColourCalculator(imageName2).toString());
        logger.info(new ColourCalculator(imageName3).toString());
        logger.info(new ColourCalculator(imageName4).toString());

        Assert.assertEquals(imageName1, imageName2);
    }
}
